﻿namespace MVCApp.Data.Models
{
    public enum VoivodeshipName
    {
        None,
        dolnoslaskie,
        kujawskopomorskie,
        lubelskie,
        lubuskie,
        lodzkie,
        malopolskie,
        mazowieckie,
        opolskie,
        podkarpackie,
        podlaskie,
        pomorskie,
        slaskie,
        swietokrzyskie,
        warminskomazurskie,
        wielkopolskie,
        zachodniopomorskie
    }
}
