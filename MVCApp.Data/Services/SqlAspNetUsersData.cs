﻿using MVCApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCApp.Data.Services
{
    public class SqlAspNetUsersData : IAspNetUsersData
    {
        private readonly ApplicationDbContext db;

        public SqlAspNetUsersData(ApplicationDbContext db)
        {
            this.db = db;
        }

        public UserData Get(string id)
        {
            var user = db.Users.FirstOrDefault(r => r.Id == id);
            var roles = db.Roles.Select(i => i.Name).ToList();
            string roleId = user.Roles.ElementAt(0).RoleId;

            UserData u = new UserData();
            u.Id = user.Id;
            u.Email = user.Email;
            u.UserName = user.UserName;
            u.UserRole = db.Roles.FirstOrDefault(i => i.Id == roleId).Name;
            u.Roles = roles;

            return u;
        }

        public IEnumerable<UserData> GetAll()
        {
            var roles = db.Roles.ToList();
            var users = db.Users.ToList();
            List<UserData> userList = new List<UserData>();
            foreach (var u in users)
            {
                userList.Add(Get(u.Id));
            }

            return userList;
        }

        public void Update(UserData aspNetUsers)
        {
            var user = db.Users.FirstOrDefault(r => r.Id == aspNetUsers.Id);
            var userRole = user.Roles.ElementAt(0);
            var role = db.Roles.FirstOrDefault(i => i.Name == aspNetUsers.UserRole);

            var entry = db.Entry(userRole);
            entry.State = EntityState.Deleted;
            db.SaveChanges();

            //user.Roles.Remove(userRole);
            //db.SaveChanges();
            //user.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole() { RoleId = role.Id, UserId = user.Id });

            entry = db.Entry(new Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole() { RoleId = role.Id, UserId = user.Id });
            entry.State = EntityState.Added;
            db.SaveChanges();
        }
    }
}
