﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MVCApp.Data.Models;

namespace MVCApp.Data.Services
{
    public class SqlMissingPeoplesData : IMissingPeoplesData
    {
        private readonly ApplicationDbContext db;

        public SqlMissingPeoplesData(ApplicationDbContext db)
        {
            this.db = db;
        }

        public void Add(MissingPeoples missingPeoples)
        {
            db.MissingPeoples.Add(missingPeoples);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            var missingpeoples = db.MissingPeoples.Find(id);
            db.MissingPeoples.Remove(missingpeoples);
            db.SaveChanges();
        }

        public MissingPeoples Get(int id)
        {
            return db.MissingPeoples.FirstOrDefault(r => r.Id == id);
        }

        public IEnumerable<MissingPeoples> GetAll()
        {
            return from r in db.MissingPeoples
                   orderby r.FirstName
                   select r;
        }

        public void Update(MissingPeoples missingPeoples)
        {
            //var r = Get(missingPeoples.Id);
            //r.FirstName = "";

            var entry = db.Entry(missingPeoples);
            entry.State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
