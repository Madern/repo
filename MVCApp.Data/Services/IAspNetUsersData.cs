﻿using MVCApp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVCApp.Data.Services
{
    public interface IAspNetUsersData
    {
        IEnumerable<UserData> GetAll();
        UserData Get(string Id);
        void Update(UserData aspNetUsers);
    }
}
