﻿using MVCApp.Data.Models;
using System.Collections.Generic;

namespace MVCApp.Data.Services
{
    public interface IMissingPeoplesData
    {
        IEnumerable<MissingPeoples> GetAll();
        MissingPeoples Get(int id);
        void Add(MissingPeoples missingPeoples);
        void Update(MissingPeoples missingPeoples);
        void Delete(int id);
    }
}
