﻿using MVCApp.Data.Models;
using MVCApp.Data.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace MVCWebApp.Api
{
    public class MissingPeopleController : ApiController
    {
        private readonly IMissingPeoplesData db;

        public MissingPeopleController(IMissingPeoplesData db)
        {
            this.db = db;
        }

        public IEnumerable<MissingPeoples> Get()
        {
            var model = db.GetAll();
            return model;
        }
    }
}
