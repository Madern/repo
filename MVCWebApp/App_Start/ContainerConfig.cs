﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using MVCApp.Data.Models;
using MVCApp.Data.Services;
using System.Web.Http;
using System.Web.Mvc;

namespace MVCWebApp
{
    public class ContainerConfig
    {
        internal static void RegisterContainer(HttpConfiguration httpConfiguration)
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterApiControllers(typeof(MvcApplication).Assembly);

            //this is for in memeory data
            //builder.RegisterType<InMemoryMissingPeoplesData>()
            //       .As<IMissingPeoplesData>()
            //       .SingleInstance();

            //this is for data in real database
            builder.RegisterType<SqlMissingPeoplesData>()
                   .As<IMissingPeoplesData>()
                   .InstancePerHttpRequest();
            //builder.RegisterType<MissingPeoplesDbContext>().InstancePerHttpRequest();

            builder.RegisterType<SqlAspNetUsersData>()
                   .As<IAspNetUsersData>()
                   .InstancePerHttpRequest();
            builder.RegisterType<ApplicationDbContext>().InstancePerHttpRequest();


            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            httpConfiguration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}