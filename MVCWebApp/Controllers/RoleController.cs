﻿using MVCApp.Data.Models;
using MVCApp.Data.Services;
using PagedList;
using System;
using System.Linq;
using System.Web.Mvc;

namespace MVCWebApp.Controllers
{
    public class RoleController : Controller
    {
        private readonly IAspNetUsersData db;

        public RoleController(IAspNetUsersData db)
        {
            this.db = db;
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Index(string sortOrder, string currentFilter, string searchStr, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NSortParm = String.IsNullOrEmpty(sortOrder) ? "First_Name_desc" : "";
            ViewBag.LSortParm = sortOrder == "L_Name" ? "Last_Name_desc" : "L_Name";

            if (searchStr != null)
            {
                page = 1;
            }
            else
            {
                searchStr = currentFilter;
            }

            ViewBag.CurrentFilter = searchStr;

            var users = from p in db.GetAll()
                          select p;

            if (!String.IsNullOrEmpty(searchStr))
            {
                users = users.Where(s => s.UserName.ToUpper().Contains(searchStr.ToUpper())
                                       || s.Email.ToUpper().Contains(searchStr.ToUpper()));
            }

            switch (sortOrder)
            {
                case "First_Name_desc":
                    users = users.OrderByDescending(s => s.UserName);
                    break;
                case "L_Name":
                    users = users.OrderBy(s => s.Email);
                    break;
                case "Last_Name_desc":
                    users = users.OrderByDescending(s => s.UserName);
                    break;
                default:
                    users = users.OrderBy(s => s.Email);
                    break;
            }

            int pageSize = 3;
            int pageNumber = (page ?? 1);

            return View(users.ToPagedList(pageNumber, pageSize));
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Details(string id)
        {
            var model = db.Get(id);
            if (model == null)
            {
                return View("NotFound");
            }
            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Edit(string id)
        {
            var model = db.Get(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, User")]
        public ActionResult Edit(UserData users)
        {
            if (ModelState.IsValid)
            {
                db.Update(users);
                TempData["Message"] = "You have succesfully saved the data!";
                return RedirectToAction("Details", new { id = users.Id });
            }
            return View(users);
        }
    }
}