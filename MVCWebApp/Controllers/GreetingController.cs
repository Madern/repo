﻿using MVCWebApp.Models;
using System.Configuration;
using System.Web.Mvc;

namespace MVCWebApp.Controllers
{
    [AllowAnonymous]
    public class GreetingController : Controller
    {
        // GET: Greeting
        public ActionResult Index(string name)
        {
            var model = new GreetingViewModel();
            //var name = HttpContext.Request.QueryString["name"];
            model.Name = name ?? "Stranger";
            model.Message = ConfigurationManager.AppSettings["message"];
            return View(model);
        }
    }
}