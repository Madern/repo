﻿namespace MVCWebApp.Models
{
    public class GreetingViewModel
    {
        public string Message { get; set; }
        public string Name { get; set; }
    }
}